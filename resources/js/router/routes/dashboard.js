export default [
  {
    path: '/',
    name: 'dashboard',
    component: () => import('@/views/Dashboard.vue'),
    meta: {
      layout: 'DashboardLayout',
      requiresAuth: true,
    },
  },
];
