import { defineStore } from 'pinia';

type Layout = 'DashboardLayout' | 'BasicLayout';

interface State {
  layout: Layout,
};

export const useLayoutStore = defineStore({
  id: 'layout',
  state: (): State => ({
    layout: 'BasicLayout',
  }),
  actions: {
    setLayout(layout: Layout) {
      this.layout = layout;
    },
  },
});
