import { defineStore } from 'pinia';
import AuthService from '../../services/AuthService';
import { to } from '../../utils/to';

interface State {
  user: object | null,
  isAuthenticated: boolean,
};

export const useAuthStore = defineStore({
  id: 'auth',
  state: (): State => ({
    user: null,
    isAuthenticated: false,
  }),
  actions: {
    async fetchUser() {
      const [err, res] = await to(AuthService.fetchUser());

      if (err) {
        this.isAuthenticated = false;

        return console.error(err);
      }

      this.isAuthenticated = true;
      this.user = res.data.user;
    },
    async login(credentials) {
      const [err, res] = await to(AuthService.login(credentials));

      if (err) {
        this.isAuthenticated = false;
        this.user = {};

        return console.error(err);
      }

      this.isAuthenticated = true;
      this.user = res.data.user;
    },
  },
});
