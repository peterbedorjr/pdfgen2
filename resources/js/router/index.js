import { createRouter, createWebHistory } from 'vue-router';

import { useLayoutStore } from '@/store/modules/layout.ts';
import { useAuthStore } from '@/store/modules/auth.ts';
import auth from './routes/auth.js'
import dashboard from './routes/dashboard.js';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    ...auth,
    ...dashboard,
  ],
});

router.beforeEach(async (to, from, next) => {
  const { layout } = to.meta;

  if (layout) {
    useLayoutStore().setLayout(layout);
  }

  if (to.meta.guest === true) {
    return next();
  }

  const authStore = useAuthStore();

  if (!authStore.isAuthenticated && !authStore.user) {
    await authStore.fetchUser();
  }

  if (to.meta.requiresAuth && !authStore.isAuthenticated) {
    return next({ name: 'auth.login' });
  }

  next();
});

export default router;
