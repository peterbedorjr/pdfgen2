<?php

use Illuminate\Support\Facades\Route;

Route::get('{any}', [\App\Http\Controllers\SpaController::class, 'index'])
    ->where('any', '.*');
