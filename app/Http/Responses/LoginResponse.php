<?php

namespace App\Http\Responses;

class LoginResponse extends \Laravel\Fortify\Http\Responses\LoginResponse
{
    public function toResponse($request)
    {
        return response()->json([
            'message' => 'Logged in successfully',
            'user' => auth()->user(),
        ]);
    }
}
