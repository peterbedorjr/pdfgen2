import axios from 'axios';

const instance = axios.create({
  baseURL: `/api/${import.meta.env.VITE_API_VERSION}`,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
});

export default instance;
