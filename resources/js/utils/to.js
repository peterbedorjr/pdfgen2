/**
 * Better handling of promise and/or async/await
 *
 * @param promise
 * @returns {Promise<unknown>}
 */
export const to = (promise) => new Promise((resolve) => promise.then((res) => resolve([null, res]))
    .catch((err) => resolve([err, null])));
