export default [
  {
    path: '/login',
    name: 'auth.login',
    component: () => import('@/views/auth/Login.vue'),
    meta: {
      layout: 'BasicLayout',
    },
  },
];
